package com.ncitguys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.list;


/**
 * DiceExpressionParserTest
 * JDiceParser
 * <p>
 * Created by Tevis Money on 2017-04-10.
 * Copyright © 2017 Tevis Money. All rights reserved.
 * Licensed under the BSD 3-Clause License, see LICENSE file for details
 */
public class DiceExpressionParserTest {

    private DiceExpressionParser parser;

    @BeforeEach
    public void setUp() {
        parser = new DiceExpressionParser();
    }

    @Test
    public void testAddition() {
        String expression = "1+1";
        DiceExpressionResult expressionResult = parser.evaluateExpression(expression);

        assertThat(expressionResult.getResult()).isEqualTo(2);
    }

    @Test
    public void testSubtraction() {
        String expression = "10-5";
        DiceExpressionResult expressionResult = parser.evaluateExpression(expression);

        assertThat(expressionResult.getResult()).isEqualTo(5);
    }

    @Test
    public void testMultiplication() {
        String expression = "2*3";
        DiceExpressionResult expressionResult = parser.evaluateExpression(expression);

        assertThat(expressionResult.getResult()).isEqualTo(6);
    }

    @Test
    public void testDivision() {
        String expression = "6/2";
        DiceExpressionResult expressionResult = parser.evaluateExpression(expression);

        assertThat(expressionResult.getResult()).isEqualTo(3);
    }

    @Test
    public void testDiceExpression() {
        String expression = "2d6";
        DiceExpressionResult result = parser.evaluateExpression(expression);

        assertThat(result.getResult()).isBetween(2, 12);
    }

    @Test
    public void testOrderOfOperations() {
        String expression = "3*(2 + 1)- 2/2";
        DiceExpressionResult result = parser.evaluateExpression(expression);

        assertThat(result.getResult()).isEqualTo(8);
    }

    @Test
    public void testReturnSelf() {
        DiceExpressionResult result = parser.evaluateExpression("1");

        assertThat(result.getResult()).isEqualTo(1);
    }

    @Test
    public void testDiceRollsInWorkStack() {
        DiceExpressionResult result = parser.evaluateExpression("3d6", true, false);

        assertThat(result.getWorkStack()).hasSize(1);
    }

    @Test
    public void testNestedSubExpressions() {
        String expression = "(1 * (3+3)) / (5 - (1 *1) - 2 )";
        DiceExpressionResult result = parser.evaluateExpression(expression, false, true);

        assertThat(result.getResult()).isEqualTo(3);
    }

    @Test
    public void testShowWork() {
        String expression = "(1 * (3+3)) / (5 - (1 *1) - 2 )";
        DiceExpressionResult result = parser.evaluateExpression(expression, false, true);

        assertThat(result.getWorkStack()).hasSize(14);
    }

    @Test
    public void testSubExpressionError() {
        String expression = "(1 * (4s5))";
        DiceExpressionResult result = parser.evaluateExpression(expression, false, true);

        assertThat(result.getResult()).isNull();
        assertThat(result.getWorkStack()).contains(
                "Result from sub expression \"1 * (4s5)\" resulted in an error."
        );
    }

    @Test
    public void testInvalidOperatorLocation() {
        String expression = "2 + * 3";
        DiceExpressionResult result = parser.evaluateExpression(expression, false, true);

        assertThat(result.getResult()).isEqualTo(5);
        assertThat(result.getWorkStack()).contains(
                "Encountered an operator token (*) at an invalid location: skipping token."
        );
    }

    @Test
    public void testMissingOpenParenthesis() {
        DiceExpressionResult result = parser.evaluateExpression("1 + (2 * (3 ) + 4", false, true);

        assertThat(result.getResult()).isNull();
        assertThat(result.getWorkStack()).contains(
                "Error parsing parentheses, unable to find close parenthesis"
        );
    }

    @Test
    public void testInvalidOperator() throws NoSuchMethodException {
        Method collapseExpressionMethod = DiceExpressionParser.class.getDeclaredMethod("collapseExpression", int.class,
                                                                                       String.class, int.class);
        collapseExpressionMethod.setAccessible(true);

        assertThatThrownBy(() -> collapseExpressionMethod.invoke(parser, 1, "q", 1))
                .hasCauseInstanceOf(StackCollapseException.class);
    }

    @Test
    public void testExpressionStackTooSmall() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method collapseStackMethod = DiceExpressionParser.class.getDeclaredMethod("collapseStack", Stack.class);
        collapseStackMethod.setAccessible(true);

        Stack<String> expressionStack = new Stack<>();
        expressionStack.push("1");
        expressionStack.push("+");

        Object result = collapseStackMethod.invoke(parser, expressionStack);
        assertThat(result).isNull();
    }

    @Test
    public void testNonIntegerSecondOperand() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method collapseStackMethod = DiceExpressionParser.class.getDeclaredMethod("collapseStack", Stack.class);
        collapseStackMethod.setAccessible(true);

        Stack<String> expressionStack = new Stack<>();
        expressionStack.push("1");
        expressionStack.push("+");
        expressionStack.push("Q");

        Object result = collapseStackMethod.invoke(parser, expressionStack);
        assertThat(result).isNull();
    }

    @Test
    public void testNonIntegerFirstOperand() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, NoSuchFieldException {
        Field showWork = DiceExpressionParser.class.getDeclaredField("showWork");
        showWork.setAccessible(true);

        Field workStack = DiceExpressionParser.class.getDeclaredField("workStack");
        workStack.setAccessible(true);

        showWork.set(parser, true);
        Method collapseStackMethod = DiceExpressionParser.class.getDeclaredMethod("collapseStack", Stack.class);
        collapseStackMethod.setAccessible(true);

        Stack<String> expressionStack = new Stack<>();
        expressionStack.push("Q");
        expressionStack.push("+");
        expressionStack.push("1");

        Object result = collapseStackMethod.invoke(parser, expressionStack);
        assertThat(result).isNull();
        assertThat(workStack.get(parser))
                .asInstanceOf(list(String.class))
                .anySatisfy(s -> assertThat(s)
                        .startsWith("Attempted to collapse expression stack, found non integer first operand:"));
    }

    @Test
    public void testMalformedExpression() {
        String expression = "1 2 3 + 6";
        DiceExpressionResult result = parser.evaluateExpression(expression);

        assertThat(result.getResult()).isNull();
    }

}
