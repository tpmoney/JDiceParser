package com.ncitguys;

/**
 *  DiceParsingOperator.java
 *  JDiceParser
 *
 *  Created by Tevis Money on 2017-04-09.
 *  Copyright © 2017 Tevis Money. All rights reserved.
 *  Licensed under the BSD 3-Clause License, see LICENSE file for details
 */
public enum DiceParsingOperator {
    UNKNOWN,
    ADD_SUBTRACT,
    MULTIPLY_DIVIDE,
    DICE;

}