package com.ncitguys;

/**
 * StackCollapseException.java
 * JDiceParser
 *
 *  Created by Tevis Money on 2017-04-09.
 *  Copyright © 2017 Tevis Money. All rights reserved.
 *  Licensed under the BSD 3-Clause License, see LICENSE file for details
 *
 *  Exception thrown by the dice expression parser if there is a problem collapsing the expression stack
 */
public class StackCollapseException extends Exception {
        public StackCollapseException(String message){
            super(message);
        }
}
