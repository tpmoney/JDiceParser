package com.ncitguys;

import java.util.List;

/**
 * DiceExpressionResult.java
 * JDiceParser
 * <p>
 * Created by Tevis Money on 2017-04-09.
 * Copyright © 2017 Tevis Money. All rights reserved.
 * Licensed under the BSD 3-Clause License, see LICENSE file for details
 */
public class DiceExpressionResult {

    private final List<DiceRollGroup> rolls;
    private final List<String>        workStack;
    private final Integer             result;

    DiceExpressionResult(Integer result, List<DiceRollGroup> rolls, List<String> workStack) {
        this.rolls = rolls;
        this.workStack = workStack;
        this.result = result;
    }

    public List<String> getWorkStack() {
        return workStack;
    }

    public Integer getResult() {
        return result;
    }

    public List<DiceRollGroup> getRolls() {
        return rolls;
    }
}
